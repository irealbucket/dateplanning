//
//  ProposalViewController.swift
//  datePanning
//
//  Created by 住原達也 on 2016/07/21.
//  Copyright © 2016年 tatsuya sumihara. All rights reserved.
//

import UIKit

class ProposalViewController: UIViewController {
    
    @IBOutlet weak var no1: UILabel!
    @IBOutlet weak var no2: UILabel!
    @IBOutlet weak var no3: UILabel!
    @IBOutlet weak var no4: UILabel!
    @IBOutlet weak var no5: UILabel!
    
    @IBOutlet weak var genre1: UILabel!
    @IBOutlet weak var genre2: UILabel!
    @IBOutlet weak var genre3: UILabel!
    @IBOutlet weak var genre4: UILabel!
    @IBOutlet weak var genre5: UILabel!
    
    @IBOutlet weak var name1: UILabel!
    @IBOutlet weak var name2: UILabel!
    @IBOutlet weak var name3: UILabel!
    @IBOutlet weak var name4: UILabel!
    @IBOutlet weak var name5: UILabel!
    
    // 前画面で選択されたジャンルのリスト
    var genreList:Array<String> = []
    
    var functionEndFlg = true
    var nameMap: Dictionary<String, String> = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for genre in genreList {
            getDatePlan(genre,callback: setMapValue)
            
        }
        
    }
    
    
//    func hogeMethod(callback: (Dictionary<String, String>) -> Void) -> Void {
//        
//        callback(self.getDatePlan())
//    }
    
    
    func setMapValue(genre:String, value:String) {
        self.nameMap[genre] = value
    }
    
    func getDatePlan(genre: String, callback: (String, String) -> Void) -> Void {
    
        // クラスを指定
        let query = NCMBQuery(className: "date_spot")

        // 前画面で選択されたジャンルを条件に設定
        query.whereKey("genre", equalTo: genre)
            

        // 設定した条件で検索
        query.findObjectsInBackgroundWithBlock { objects, error in
    
            if (error != nil){
                // 検索失敗時の処理

            } else {
                // 検索成功時の処理
                let size:Int = objects.count
                let num = Int(arc4random_uniform(UInt32(size)))
                let objectId: String! = (objects[num].objectForKey("objectId") as? String)!
                let spotName: String! = (objects[num].objectForKey("name") as? String)!

                //nameMap[genre] = spotName + ":" + objectId
                callback(genre, spotName + ":" + objectId)
            }
        }
        
//       let runLoop = NSRunLoop.currentRunLoop()
//        while nameMap.count <= count &&
//            runLoop.runMode(NSDefaultRunLoopMode, beforeDate: NSDate(timeIntervalSinceNow: 0.1)) {
//                print("wait")
//        }
        
        //callback(nameMap)
        
      //  return nameMap
    }
    
    func setViewValue(nameMap : Dictionary<String, String>) {
        var count = 1
        for genre in genreList {
            
            if (count == 1) {
                self.no1.text = String(count)
                self.genre1.text = genre
                self.name1.text = nameMap[genre]!.componentsSeparatedByString(":")[0]
                
            } else if (count == 2) {
                self.no2.text = String(count)
                self.genre2.text = genre
                self.name2.text = nameMap[genre]!.componentsSeparatedByString(":")[0]
                
            } else if (count == 3) {
                self.no3.text = String(count)
                self.genre3.text = genre
                self.name3.text = nameMap[genre]!.componentsSeparatedByString(":")[0]
                
            } else if (count == 4) {
                self.no4.text = String(count)
                self.genre4.text = genre
                self.name4.text = nameMap[genre]!.componentsSeparatedByString(":")[0]
                
            } else if (count == 5) {
                self.no5.text = String(count)
                self.genre5.text = genre
                self.name5.text = nameMap[genre]!.componentsSeparatedByString(":")[0]
            }
            
            count += 1
        }
    }
    
    

//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        if (segue.identifier == "proposalConfirmView") {
//            var proposalAddViewController = segue.destinationViewController as! ProposalAddViewController
//            proposalAddViewController.nameMap = self.nameMap
//        }
//        
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
