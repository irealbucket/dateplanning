//
//  AddPhotoViewController.swift
//  datePanning
//
//  Created by 住原達也 on 2016/07/24.
//  Copyright © 2016年 tatsuya sumihara. All rights reserved.
//

import UIKit

class AddPhotoViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    
    /** 選択画像 */
    @IBOutlet weak var imageFromCameraRoll: UIImageView!
    
    /** テキストエリア */
    @IBOutlet weak var textView: UITextView!
    
    
    // 前画面の項目を引き継ぎ、値を入れるための変数
    var spotGenre : String!
    var spotName : String!
    var spotAddress : String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageFromCameraRoll.image = UIImage(named: "noimage.gif")
        textView.layer.borderWidth = 1
        textView.layer.cornerRadius = 8;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    /**
     写真選択ボタンが押下された時の処理
     */
    @IBAction func onClickPhotoChoice(sender: AnyObject) {
        //ライブラリから写真を選択する
        pickImageFromLibrary()
    }

    
    @IBAction func tapScreen(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)

    }
    
    
    /**
     登録ボタンが押下された時の処理
     */
        // 対象クラスのオブジェクトを取得
    
    @IBAction func onClickSubmit(sender: AnyObject) {
        let spot = NCMBObject(className: "date_spot")
        
        //保存対象の画像ファイルを作成する
        var saveError: NSError? = nil
        
//        var scale:CGFloat = 1.0
//        var width:CGFloat = 0
//        var height:CGFloat = 0
//        var screenWidth:CGFloat = 0
//        var screenHeight:CGFloat = 0
//        
//        if(width*scale > screenWidth/10){
//            scale -= 2
//        }
//        
//        let rect:CGRect = CGRectMake(0, 0, width*scale, height*scale)
//        imageFromCameraRoll.frame = rect;
//        imageFromCameraRoll.center = CGPointMake(187.5, 333.5)
//        self.view.addSubview(imageFromCameraRoll)
        
        let newCgIm = CGImageCreateCopy(imageFromCameraRoll.image?.CGImage)
        let newImage = UIImage(CGImage: newCgIm!, scale: imageFromCameraRoll.image!.scale, orientation: imageFromCameraRoll.image!.imageOrientation)
        let size = CGSize(width: 100, height: 100)
        UIGraphicsBeginImageContext(size)
        newImage.drawInRect(CGRectMake(0, 0, size.width, size.height))
        let resizeImage = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = UIImagePNGRepresentation(resizeImage)
        
        let targetFile = NCMBFile.fileWithData(imageData) as! NCMBFile
        
        spot.setObject(spotName, forKey: "name")
        spot.setObject(spotGenre, forKey: "genre")
        // ファイル名を保存
        spot.setObject(targetFile.name, forKey: "photo")
        spot.setObject(textView.text, forKey: "memo")
        spot.setObject(spotAddress, forKey: "address")
        spot.save(&saveError)
        
        
        
        //ファイルはバックグラウンド実行をする
        targetFile.saveInBackgroundWithBlock({ (error: NSError!) -> Void in
            
            if error == nil {
                print("画像データ保存完了: \(targetFile.name)")

            } else {
                print("アップロード中にエラーが発生しました: \(error)")
            }
            
            }, progressBlock: { (percentDone: Int32) -> Void in
                
                // 進捗状況を取得します。保存完了まで何度も呼ばれます
                print("進捗状況: \(percentDone)% アップロード済み")
        })
        
        if saveError == nil {
            print("success save data.")
        } else {
            print("failure save data. \(saveError)")
        }
        
    }
    
    
    /**
     ライブラリから写真を選択する
     */
    func pickImageFromLibrary() {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {    //追記
            
            //写真ライブラリ(カメラロール)表示用のViewControllerを宣言しているという理解
            let controller = UIImagePickerController()
            
            controller.delegate = self
            
            //新しく宣言したViewControllerでカメラとカメラロールのどちらを表示するかを指定
            //以下はカメラロールの例
            //.Cameraを指定した場合はカメラを呼び出し(シミュレーター不可)
            controller.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            
            //新たに追加したカメラロール表示ViewControllerをpresentViewControllerにする
            self.presentViewController(controller, animated: true, completion: nil)
        }
    }
    
    /**
     写真を選択した時に呼ばれる
     */
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo: [String: AnyObject]) {
        
        //このif条件はおまじないという認識で今は良いと思う
        if didFinishPickingMediaWithInfo[UIImagePickerControllerOriginalImage] != nil {
            
            //didFinishPickingMediaWithInfo通して渡された情報(選択された画像情報が入っている？)をUIImageにCastする
            //そしてそれを宣言済みのimageViewへ放り込む
            imageFromCameraRoll.image = didFinishPickingMediaWithInfo[UIImagePickerControllerOriginalImage] as? UIImage
            
        }
        
        //写真選択後にカメラロール表示ViewControllerを引っ込める動作
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    

    
    
    

}
