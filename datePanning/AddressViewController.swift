import UIKit
import MapKit

class AddressViewController: UIViewController{
    
    
    @IBOutlet weak var map: MKMapView!
    var address:String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        putPinAddress(self.address!)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func putPinAddress(address: String) -> Bool {
        self.view.endEditing(true)
        
        let myGeocoder:CLGeocoder = CLGeocoder()
        
        //住所
        let searchStr = address
        
        //住所を座標に変換する。
        myGeocoder.geocodeAddressString(searchStr, completionHandler: {(placemarks, error) in
            
            if(error == nil) {
                for placemark in placemarks! {
                    let location:CLLocation = placemark.location!
                    
                    //中心座標
                    let center = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
                    
                    //表示範囲
                    let span = MKCoordinateSpanMake(0.01, 0.01)
                    
                    //中心座標と表示範囲をマップに登録する。
                    let region = MKCoordinateRegionMake(center, span)
                    self.map.setRegion(region, animated:true)
                    
                    //地図にピンを立てる。
                    let annotation = MKPointAnnotation()
                    annotation.coordinate = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
                    self.map.addAnnotation(annotation)
                    
                }
            } else {
                //self.addressText.text = "検索できませんでした。"
            }
        })
        
        //改行を入れない。
        return false
    }
    

    
    
}