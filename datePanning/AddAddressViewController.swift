//
//  AddAddressViewController.swift
//  datePanning
//
//  Created by 住原達也 on 2016/07/24.
//  Copyright © 2016年 tatsuya sumihara. All rights reserved.
//

import UIKit
import MapKit



class AddAddressViewController: UIViewController, UITextFieldDelegate  {
    
    var testManager:CLLocationManager = CLLocationManager()

    
    /** ジャンル */
    @IBOutlet weak var genre: UILabel!
    var choiceGenre : String?
    
    
    /** スポット名 */
    @IBOutlet weak var name: UITextField!
    
    /** 住所入力テキスト */
    @IBOutlet weak var textField: UITextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        genre.text = choiceGenre
        //textField.enabled = false
    }

    @IBAction func tapScreen(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)

    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
 
    @IBAction func unwindToReturnAddAddress(segue: UIStoryboardSegue) {
        
    }
    
    @IBAction func unwindToSubmitAddAddress(segue: UIStoryboardSegue) {
        let mapViewController = segue.sourceViewController as! ShowMapViewController

        self.textField.text = mapViewController.addressText.text
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "address") {
            let addPhotoViewController = segue.destinationViewController as! AddPhotoViewController
            
            addPhotoViewController.spotGenre = genre.text
            addPhotoViewController.spotName = name.text;
            addPhotoViewController.spotAddress = textField.text;
        }
        
    }


}
