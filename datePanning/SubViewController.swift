import UIKit

class SubViewController: UIViewController{
    
    
    @IBOutlet weak var genre: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var memo: UILabel!
    @IBOutlet weak var selectedImg: UIImageView!
    var objectId:String? = nil
    var image:UIImageView? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        genre.text = objectId
        selectedImg.image = image?.image
        getDateSpot()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func getDateSpot(){
        // クラスを指定し、取得する値の条件する
        let query = NCMBQuery(className: "date_spot")
        query.whereKey("objectId", equalTo: self.objectId)
        
        // 結果を取得（非同期）
        query.findObjectsInBackgroundWithBlock { objects, error in
            
            if error != nil {
                print("データ取得失敗: \(error)")
            } else {
                
                // 取得に成功した場合の処理
                if objects.count > 0 {
                    
                    for object in objects {
                        self.genre.text = (object.objectForKey("genre") as? String)!
                        self.name.text = (object.objectForKey("name") as? String)!
                        self.address.text = (object.objectForKey("address") as? String)!
                        self.memo.text = (object.objectForKey("memo") as? String)!
                    }
                }
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "addressView") {
            let addressViewController = segue.destinationViewController as! AddressViewController
            
            addressViewController.address = address.text
            
        }
        
    }
    
    @IBAction func unwindToSubView(segue: UIStoryboardSegue) {
        
    }
    
    
}