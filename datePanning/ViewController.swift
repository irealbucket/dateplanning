//
//  ViewController.swift
//  datePanning
//
//  Created by 住原達也 on 2016/07/11.
//  Copyright © 2016年 tatsuya sumihara. All rights reserved.
//

import UIKit


class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    @IBOutlet weak var table: UITableView!
    
    private var ActivityIndicator: UIActivityIndicatorView!

    
    /// 画像のファイル名
    var imageNames:[UIImage] = []
    
    var objectIdList: [String] = []
    var genreList: [String] = []
    var nameList: [String] = []
    var nameMap: Dictionary<String, String> = [:]

    
    // ページの高さ
    var pHeight:CGFloat!
    // ページの幅
    var pWidth:CGFloat!
    
    // Totalのページ数
    var pNum:Int = 0
    
    var count:Int = 1
    
    
    var selectedImage: UIImageView?
    
    var dateSpotArray: NSMutableArray = NSMutableArray()
    
    var objectId: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

//        self.showIndicator()

        self.getDateSpots()
        
//        self.hideIndicator()
        
        if let indexPathForSelectedRow = table.indexPathForSelectedRow {
            table.deselectRowAtIndexPath(indexPathForSelectedRow, animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func getDateSpots(){
        // クラスを指定し、取得する値の条件する
        let query = NCMBQuery(className: "date_spot")
        query.orderByDescending("createDate")
        
        // 結果を取得（非同期）
        query.findObjectsInBackgroundWithBlock { objects, error in
            
            if error != nil {
                print("データ取得失敗: \(error)")
            } else {
                
                // 取得に成功した場合の処理
                if objects.count > 0 {
                    self.pNum = objects.count
                    
                    for object in objects {
                        self.nameMap[self.count.description] = ""
                        let objectId: String = (object.objectForKey("objectId") as? String)!
                        let genre: String = (object.objectForKey("genre") as? String)!
                        let name: String = (object.objectForKey("name") as? String)!
                        let photoName: String = (object.objectForKey("photo") as? String)!
                        
                        self.objectIdList.append(objectId)
                        self.genreList.append(genre)
                        self.nameList.append(name)
                        
                        
                        let fileData = NCMBFile.fileWithName(photoName, data: nil) as! NCMBFile
                        let screenSize: CGRect = UIScreen.mainScreen().bounds
                        self.pWidth = screenSize.width
                        let imageData: NSData? = try? fileData.getData()
                        if imageData != nil {
                            let image = UIImage(data: imageData!)
                            let width = image!.size.width
                            let height = image!.size.height
                            self.pHeight = self.pWidth * height/width
                            let imageView = UIImageView(image:image)
                            var rect:CGRect = imageView.frame
                            rect.size.height = self.pHeight
                            rect.size.width = self.pWidth
                            imageView.frame = rect
                            imageView.tag = 2
                            self.imageNames.append(imageView.image!)
                        } else {
                            let image1 = UIImage(named:"noimage.gif")
                            self.imageNames.append(image1!)
                        }
                        self.table.reloadData()
                        
 
                    }
                }
            }
        }
        
    }
    
    
   
    @IBAction func unwindToHome(segue: UIStoryboardSegue) {
        
    }
    
//    // Cell が選択された場合
//    func tableView(table: UITableView, didSelectRowAtIndexPath indexPath:NSIndexPath) {
//        
//        objectId = "\(objectIdList[indexPath.row])"
//        var image1:UIImage?
//        image1 = imageNames[indexPath.row]
//        selectedImage = UIImageView(image:image1)
//        performSegueWithIdentifier("toSubViewController",sender: nil)
//        
//    }
    
    
    /// セルの個数を指定するデリゲートメソッド（必須）
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pNum
    }
    
    /// セルに値を設定するデータソースメソッド（必須）
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // tableCell の ID で UITableViewCell のインスタンスを生成
        let cell = table.dequeueReusableCellWithIdentifier("tableCell", forIndexPath: indexPath)
        
        // 写真は全件を取得してから
        if (pNum == imageNames.count) {
            var image1:UIImage?
            image1 = imageNames[indexPath.row]
        
            // Tag番号 1 で UIImageView インスタンスの生成
            let imageView = table.viewWithTag(1) as! UIImageView
            imageView.image = image1
        
            imageView.image = UIImageView(image:image1).image
        }
        
        // Tag番号 2 で UILabel インスタンスの生成
        let label1 = table.viewWithTag(2) as! UILabel
        label1.text = "\(genreList[indexPath.row])"

        // Tag番号 3 で UILabel インスタンスの生成
        let label2 = table.viewWithTag(3) as! UILabel
        label2.text = "\(nameList[indexPath.row])"
        
        // Tag番号 4 で UILabel インスタンスの生成
        let label3 = table.viewWithTag(4) as! UILabel
        label3.text = "\(objectIdList[indexPath.row])"
        
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "toSubViewController") {
            let subVC: SubViewController = (segue.destinationViewController as? SubViewController)!
            subVC.objectId = self.objectId
            subVC.image = self.selectedImage
        }
    }
    
    func showIndicator(){
        //Indicatorを作成
        ActivityIndicator = UIActivityIndicatorView()
        ActivityIndicator.frame = CGRectMake(0, 0, 100, 100)
        ActivityIndicator.backgroundColor = UIColor(red: 0/2555, green: 0/255, blue: 0/255, alpha: 0.7)
        ActivityIndicator.layer.cornerRadius = 8
        ActivityIndicator.center = self.view.center
        
        //Indicatorの状態を管理
        ActivityIndicator.hidesWhenStopped = false
        ActivityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.White
        
        //クルクルを開始
        ActivityIndicator.startAnimating()
        
        //Viewに追加
        self.view.addSubview(ActivityIndicator)
    }
    
    func hideIndicator(){
        ActivityIndicator.stopAnimating()
    }


}

