//
//  ProposalAddViewController.swift
//  datePlanning
//
//  Created by 住原達也 on 2016/08/24.
//  Copyright © 2016年 tatsuya sumihara. All rights reserved.
//

import UIKit

class ProposalAddViewController: UIViewController {
    
    var nameMap: Dictionary<String, String> = [:]

    override func viewDidLoad() {
        super.viewDidLoad()

        let dateTime = self.getDateTime()
        self.addDateCourseHistory(dateTime)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    func getDateTime() -> String {
        
         // 現在日時の取得
        let now = NSDate()
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US") // ロケールの設定
        dateFormatter.dateFormat = "yyyyMMddHHmmss" // 日付フォーマットの設定
        
        return dateFormatter.stringFromDate(now)
    }
    
    
    func addDateCourseHistory(dateTime: String) {
        
        // デートコースに登録
        let dateCourse = NCMBObject(className: "date_course")
        dateCourse.setObject(dateTime, forKey: "dateId")
        
        // デートコース履歴に登録
        let dateCourseDetail = NCMBObject(className: "date_course_detail")
        var saveError: NSError? = nil
        var count = 1
        for key in nameMap.keys {
            let dateSpotId: String! = nameMap[key]!.componentsSeparatedByString(":")[1]
            dateCourseDetail.setObject(dateTime, forKey: "dateId")
            dateCourseDetail.setObject(dateSpotId, forKey: "dateSpotId")
            dateCourseDetail.setObject(count, forKey: "seqNo")
            dateCourseDetail.save(&saveError)
            if saveError == nil {
                print("success save data.")
            } else {
                print("failure save data. \(saveError)")
            }
            count += 1
        }
    }
}
