//
//  AddGenreChoiceViewController.swift
//  datePanning
//
//  Created by 住原達也 on 2016/07/18.
//  Copyright © 2016年 tatsuya sumihara. All rights reserved.
//

import UIKit
import CoreLocation

class AddGenreChoiceViewController: UIViewController, CLLocationManagerDelegate {
    
    var genre : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickEat(sender: AnyObject) {
        genre = "食事"
    }
    
    @IBAction func onClickAmusement(sender: AnyObject) {
        genre = "アミューズメント"
    }
    
    @IBAction func onClickWalk(sender: AnyObject) {
        genre = "散歩"
    }
    
    @IBAction func onClickView(sender: AnyObject) {
        genre = "観賞"
    }
    
    @IBAction func onClickLightFood(sender: AnyObject) {
        genre = "軽食"
    }
    
    @IBAction func onClickShopping(sender: AnyObject) {
        genre = "ショッピング"
    }
    
    @IBAction func onClickRelaxation(sender: AnyObject) {
        genre = "リラクゼーション"
    }
    
    @IBAction func onClickNightView(sender: AnyObject) {
        genre = "夜景"
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "next") {
            var addAddressViewController = segue.destinationViewController as! AddAddressViewController
            
            addAddressViewController.choiceGenre = genre
        }
       
    }
    
    @IBAction func unwindToAddGenreChoice(segue: UIStoryboardSegue) {
        
    }


}
