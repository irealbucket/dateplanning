//
//  ShowMapViewController.swift
//  datePlanning
//
//  Created by 住原達也 on 2016/08/28.
//  Copyright © 2016年 tatsuya sumihara. All rights reserved.
//

import Foundation
import MapKit


import UIKit

class ShowMapViewController: UIViewController, UITextFieldDelegate   {
    
    
    @IBOutlet weak var addressText: UITextField!
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        //デリゲート先に自分を設定する。
        addressText.delegate = self
        
        //TODO 現在地
        let center = CLLocationCoordinate2DMake(34.680626, 135.501941)
        
        //表示範囲
        let span = MKCoordinateSpanMake(0.01, 0.01)
        
        //中心座標と表示範囲をマップに登録する。
        let region = MKCoordinateRegionMake(center, span)
        self.mapView.setRegion(region, animated:true)
        
    }
    

    @IBAction func pressMap(sender: UILongPressGestureRecognizer) {
        
        // マップに立っているピンを削除
        self.mapView.removeAnnotations(self.mapView.annotations)
        
        // 住所入力テキストに入っている値を削除
        self.addressText.text = ""
        
        var annotation:MKPointAnnotation!
        
        //マップビュー内のタップした位置を取得する
        let location:CGPoint = sender.locationInView(mapView)
        
        //長押し終了
        if (sender.state == UIGestureRecognizerState.Ended){
            
            //タップした位置を緯度、経度の座標に変換する
            let mapPoint:CLLocationCoordinate2D = mapView.convertPoint(location, toCoordinateFromView: mapView)
            let location = CLLocation(latitude:mapPoint.latitude, longitude: mapPoint.longitude)
            
            //座標を住所に変換する。
            let myGeocoder:CLGeocoder = CLGeocoder()
            myGeocoder.reverseGeocodeLocation(location, completionHandler: {(placemarks, error) in
                
                if(error == nil) {
                    for placemark in placemarks! {
                        
                        //ピンを地図に刺す
                        annotation = MKPointAnnotation()
                        annotation.coordinate = CLLocationCoordinate2DMake(mapPoint.latitude, mapPoint.longitude)
                        
                        
                        // 座標が正確に取得できているか確認
                        if (placemark.administrativeArea != nil && placemark.locality != nil && placemark.thoroughfare != nil && placemark.subThoroughfare != nil) {
                            // 座標が正確に取得できている場合
                            self.addressText.text = "\(placemark.administrativeArea!)\(placemark.locality!)\(placemark.thoroughfare!)\(placemark.subThoroughfare!)"
                        } else {
                            // 座標が正確に取得できていない場合
                            self.addressText.text = "住所不明"
                        }
                        
                        self.mapView.addAnnotation(annotation)
                    }
                } else {
                    self.addressText.text = "住所不明"
                }
            })
        }
        
    }
    
    @IBAction func tapScreen(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)

    }
   
    
    /** 住所入力後のReturnキー押下時の呼び出しメソッド　*/
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        let myGeocoder:CLGeocoder = CLGeocoder()
        
        //住所
        let searchStr = textField.text
        
        //住所を座標に変換する。
        myGeocoder.geocodeAddressString(searchStr!, completionHandler: {(placemarks, error) in
            
            if(error == nil) {
                for placemark in placemarks! {
                    let location:CLLocation = placemark.location!
                    
                    //中心座標
                    let center = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
                    
                    //表示範囲
                    let span = MKCoordinateSpanMake(0.01, 0.01)
                    
                    //中心座標と表示範囲をマップに登録する。
                    let region = MKCoordinateRegionMake(center, span)
                    self.mapView.setRegion(region, animated:true)
                    
                    //地図にピンを立てる。
                    let annotation = MKPointAnnotation()
                    annotation.coordinate = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
                    self.mapView.addAnnotation(annotation)
                    
                }
            } else {
                self.addressText.text = "検索できませんでした。"
            }
        })
        
        //改行を入れない。
        return false
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "map") {
            let addAddressViewController = segue.destinationViewController as! AddAddressViewController
            
            addAddressViewController.textField.text = addressText.text
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
}