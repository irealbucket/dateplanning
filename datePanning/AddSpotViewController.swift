//
//  AddSpotViewController.swift
//  datePanning
//
//  Created by 住原達也 on 2016/07/18.
//  Copyright © 2016年 tatsuya sumihara. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation


class AddSpotViewController: UIViewController , UIImagePickerControllerDelegate, UINavigationControllerDelegate,
    CLLocationManagerDelegate,
    MKMapViewDelegate,
    UITextFieldDelegate
{
    
    
    @IBOutlet weak var testMapView: MKMapView!
    
    @IBOutlet weak var testTextField: UITextField!
    
    
    @IBAction func pressMap(sender: UILongPressGestureRecognizer) {
        //マップビュー内のタップした位置を取得する。
        let location:CGPoint = sender.locationInView(testMapView)
        
        //長押し終了
        if (sender.state == UIGestureRecognizerState.Ended){
            
            //タップした位置を緯度、経度の座標に変換する。
            let mapPoint:CLLocationCoordinate2D = testMapView.convertPoint(location, toCoordinateFromView: testMapView)
            let location = CLLocation(latitude:mapPoint.latitude, longitude: mapPoint.longitude)
            
            //座標を住所に変換する。
            let myGeocoder:CLGeocoder = CLGeocoder()
            myGeocoder.reverseGeocodeLocation(location, completionHandler: {(placemarks, error) in
                
                if(error == nil) {
                    for placemark in placemarks! {
                        
                        //ピンを地図に刺す。
                        let annotation = MKPointAnnotation()
                        annotation.coordinate = CLLocationCoordinate2DMake(mapPoint.latitude, mapPoint.longitude)
                        annotation.title = "住所"
                        annotation.subtitle = "\(placemark.administrativeArea!)\(placemark.locality!)\(placemark.thoroughfare!)\(placemark.subThoroughfare!)"
                        self.testMapView.addAnnotation(annotation)
                    }
                } else {
                  //  self.testTextField.text = "住所不明"
                }
            })
        }
    }
    
    @IBOutlet weak var textView: UITextView!
    
    /** 選択画像 */
    @IBOutlet weak var imageFromCameraRoll: UIImageView!

    
    
    /** ジャンル */
    @IBOutlet weak var genre: UILabel!
    var choiceGenre : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        genre.text = choiceGenre
        imageFromCameraRoll.image = UIImage(named: "noimage.gif")
        textView.layer.borderWidth = 1
        textView.layer.cornerRadius = 8;
        
        
        
        //デリゲート先に自分を設定する。
        testMapView.delegate = self
        
        //中心座標
        let center = CLLocationCoordinate2DMake(35.690553, 139.699579)
        
        //表示範囲
        let span = MKCoordinateSpanMake(0.001, 0.001)
        
        //中心座標と表示範囲をマップに登録する。
        let region = MKCoordinateRegionMake(center, span)
        self.testMapView.setRegion(region, animated:true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /**
     ライブラリから写真を選択する
     */
    func pickImageFromLibrary() {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {    //追記
            
            //写真ライブラリ(カメラロール)表示用のViewControllerを宣言しているという理解
            let controller = UIImagePickerController()
            
            //おまじないという認識で今は良いと思う
            controller.delegate = self
            
            //新しく宣言したViewControllerでカメラとカメラロールのどちらを表示するかを指定
            //以下はカメラロールの例
            //.Cameraを指定した場合はカメラを呼び出し(シミュレーター不可)
            controller.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            
            //新たに追加したカメラロール表示ViewControllerをpresentViewControllerにする
            self.presentViewController(controller, animated: true, completion: nil)
        }
    }
    
    
    /**
     写真選択ボタンが押下された時の処理
     */
    @IBAction func onClickPhotoChoice(sender: AnyObject) {
        
        //ライブラリから写真を選択する
        pickImageFromLibrary()
    }
    
    /**
     写真を選択した時に呼ばれる (swift2.0対応)
     
     :param: picker:おまじないという認識で今は良いと思う
     :param: didFinishPickingMediaWithInfo:おまじないという認識で今は良いと思う
     */
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo: [String: AnyObject]) {
        
        //このif条件はおまじないという認識で今は良いと思う
        if didFinishPickingMediaWithInfo[UIImagePickerControllerOriginalImage] != nil {
            
            //didFinishPickingMediaWithInfo通して渡された情報(選択された画像情報が入っている？)をUIImageにCastする
            //そしてそれを宣言済みのimageViewへ放り込む
            imageFromCameraRoll.image = didFinishPickingMediaWithInfo[UIImagePickerControllerOriginalImage] as? UIImage
        }
        
        //写真選択後にカメラロール表示ViewControllerを引っ込める動作
        picker.dismissViewControllerAnimated(true, completion: nil)
    }

}
