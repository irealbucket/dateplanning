import UIKit

class CustomCell: UITableViewCell {
    
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setCell(cell :CellObject) {
        self.date.text = cell.date as String
        var err: NSError?
        var imageData :NSData = NSData(contentsOfURL: cell.imageUrl!,options: NSDataReadingOptions.DataReadingMappedIfSafe, error: &err)!
        self.iconImage.image = UIImage(data:imageData)
    }
}