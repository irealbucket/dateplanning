//
//  ProposalRootViewController.swift
//  datePanning
//
//  Created by 住原達也 on 2016/07/18.
//  Copyright © 2016年 tatsuya sumihara. All rights reserved.
//

import UIKit

class ProposalRootViewController: UIViewController {
    
    @IBOutlet weak var food: UILabel!
    @IBOutlet weak var amusement: UILabel!
    @IBOutlet weak var walk: UILabel!
    @IBOutlet weak var lightFood: UILabel!
    @IBOutlet weak var shopping: UILabel!
    @IBOutlet weak var relaxation: UILabel!
    @IBOutlet weak var neightView: UILabel!
    @IBOutlet weak var views: UILabel!
    
    var foodFlg:Bool = false;
    var amusementFlg:Bool = false;
    var walkFlg:Bool = false;
    var viewFlg:Bool = false;
    var lightFoodFlg:Bool = false;
    var shoppingFlg:Bool = false;
    var relaxationFlg:Bool = false;
    var neightViewFlg:Bool = false;
    
    var array:Array<String> = []
    
    @IBAction func onClickFood(sender: AnyObject) {
        if foodFlg {
            foodFlg = false
            removeItem("食事")
            food.text = ""
        } else {
            foodFlg = true
            array += ["食事"]
        }
        setGenreSeq()
    }
    
    @IBAction func onClickAmusement(sender: AnyObject) {
        if amusementFlg {
            amusementFlg = false
            removeItem("アミューズメント")
            amusement.text = ""
            
        } else {
            amusementFlg = true
            array += ["アミューズメント"]
        }
        setGenreSeq()
    }
    
    @IBAction func onClickWalk(sender: AnyObject) {
        if walkFlg {
            walkFlg = false
            removeItem("散歩")
            walk.text = ""
            
        } else {
            walkFlg = true
            array += ["散歩"]
        }
        setGenreSeq()
    }
    
    @IBAction func onClickView(sender: AnyObject) {
        if viewFlg {
            viewFlg = false
            removeItem("観賞")
            views.text = ""
            
        } else {
            viewFlg = true
            array += ["観賞"]
        }
        setGenreSeq()
    }
    
    @IBAction func onClickLightFood(sender: AnyObject) {
        if lightFoodFlg {
            lightFoodFlg = false
            removeItem("軽食")
            lightFood.text = ""
            
        } else {
            lightFoodFlg = true
            array += ["軽食"]
        }
        setGenreSeq()
    }
    
    @IBAction func onClickShopping(sender: AnyObject) {
        if shoppingFlg {
            shoppingFlg = false
            removeItem("ショッピング")
            shopping.text = ""
            
        } else {
            shoppingFlg = true
            array += ["ショッピング"]
        }
        setGenreSeq()
    }
    
    @IBAction func onClickRelaxation(sender: AnyObject) {
        if relaxationFlg {
            relaxationFlg = false
            removeItem("リラクゼーション")
            relaxation.text = ""
            
        } else {
            relaxationFlg = true
            array += ["リラクゼーション"]
        }
        setGenreSeq()
    }
    
    @IBAction func onClickNeightView(sender: AnyObject) {
        if neightViewFlg {
            neightViewFlg = false
            removeItem("夜景")
            neightView.text = ""
            
        } else {
            neightViewFlg = true
            array += ["夜景"]
        }
        setGenreSeq()
    }
    
    /** ジャンルの順番をラベルに設定 */
    func setGenreSeq() {
        var count = 1
        for item in array {
            if (item == "食事") {
                food.text = String(count)
            } else if (item == "amusement") {
                amusement.text = String(count)
            } else if (item == "散歩") {
                walk.text = String(count)
            } else if (item == "観賞") {
                views.text = String(count)
            } else if (item == "軽食") {
                lightFood.text = String(count)
            } else if (item == "ショッピング") {
                shopping.text = String(count)
            } else if (item == "リラクゼーション") {
                relaxation.text = String(count)
            } else if (item == "夜景") {
                neightView.text = String(count)
            }
            count += 1
        }
    }
    
    func removeItem(str : String) {
        var count = 0
        var itemNum = 0
        for item in array {
            if (item == str) {
                itemNum = count
                break
            }
            count += 1
        }
        array.removeAtIndex(itemNum)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "proposalView") {
            var proposalViewController = segue.destinationViewController as! ProposalViewController
            
            
            proposalViewController.genreList = array
        }
        
    }
    
    @IBAction func unwindToProposalRoot(segue: UIStoryboardSegue) {
        
    }

}
