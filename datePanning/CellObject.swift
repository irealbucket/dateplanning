//
//  CellObject.swift
//  datePlanning
//
//  Created by 住原達也 on 2016/08/25.
//  Copyright © 2016年 tatsuya sumihara. All rights reserved.
//

import Foundation

class CellObject : NSObject {
    var date:NSString
    var imageUrl:NSURL?
    
    init(date: String, imageUrl: NSURL?){
        self.date = date
        self.imageUrl = imageUrl
    }
}